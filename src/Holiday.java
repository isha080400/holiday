public class Holiday {
    private String name;
    private int day;
    private String month;

    Holiday(String nameOfHoliday, int dayOfMonth, String nameOfMonth) {
        name = nameOfHoliday;
        day = dayOfMonth;
        month = nameOfMonth;
    }

    public String getMonth(){
        return month;
    }

    public int getDay(){
        return day;
    }

    public boolean inSameMonth(Holiday anotherHoliday){
        return this.getMonth().equals(anotherHoliday.getMonth());
    }

    public static float avgDate(Holiday[] holidays){
        float sumOfDates = 0;
        int sizeOfArray = holidays.length;

        for(int i = 0; i < sizeOfArray; i++){
            sumOfDates += holidays[i].getDay();
        }

        return sumOfDates / sizeOfArray;
    }
}
