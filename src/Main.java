public class Main {
    public static void main(String[] args){
        Holiday independenceDay = new Holiday("Independence Day", 4, "July");
        System.out.println(independenceDay.getMonth());
        Holiday diwali = new Holiday("Diwali", 5, "July");
        System.out.println(diwali.inSameMonth(independenceDay));
        Holiday holi = new Holiday("Holi", 22, "March");
        System.out.println(holi.inSameMonth(diwali));

        Holiday[] holidays = {independenceDay, diwali, holi};

        System.out.println(Holiday.avgDate(holidays));
    }
}
